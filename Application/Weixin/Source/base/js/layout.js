$(document).ready(function(){
	
	//动态设置页面布局
	var bodyH = $("body").height() - $(".header").height() - $(".footer").height();
	$(".b_content, .b_aside").css('min-height', bodyH);
	
	//优化所有的radio和checkbox
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-orange',
		radioClass: 'iradio_square-orange',
		increaseArea: '20%' // optional
	});
	
	//激活弹出层
	//$("[data-toggle='popover']").popover();
	$("[data-toggle='popover']").each(function(){
		var id = $(this).attr("id");
		var html = $(this).data("html");
		if(id && html){
			$(this).popover({
				html: html,
				container: $(this).data("container"),
				placement: $(this).data("placement"),
				content: $("#"+id+"_dialog_content").html(),
				animation: true
			});
		}else{
			$(this).popover();
		}
	});
	$("body").on("click","[data-toggle='popover'],.popover",function(){
		return false;
	});
	$("body").click(function(){
		$("[data-toggle='popover']").popover('hide');
	});
	
});