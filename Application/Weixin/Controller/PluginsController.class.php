<?php 

namespace Weixin\Controller;
use Think\Controller;

class PluginsController extends BaseController{
	
	public function __construct(){
		parent::__construct();
		$this->assign('menu','plugins');
	}
	
	public function index(){
		
		$this->display('plugins');
		
	}
	
}