<?php 

namespace Weixin\Controller;
use Think\Controller;

class IndexController extends BaseController{
	
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		
		//是否登录
		if($this->islogin){
			
			//登录后跳转用户面板
			$this->redirect('Dashboard/index');
			
		}else{
			//没登录显示网站首页
			$this->display('index');
		}
		
	}
	
}