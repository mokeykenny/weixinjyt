<?php 

namespace Weixin\Controller;
use Think\Controller;

class SmsController extends BaseController{
	
	public function __construct(){
		parent::__construct();
		$this->assign('menu','sms');
	}
	
	public function index(){
		$this->display('sms');
	}
	
	public function test(){
		
		echo "<pre>";print_r($this->easywechat->user);
	}
	
	//微信接口测试
	public function checkSignature(){
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];
		$echostr = $_GET["echostr"];
        		
		$token = "weixin";
		$tmpArr = array($token, $timestamp, $nonce);
		sort($tmpArr, SORT_STRING);
		$tmpStr = implode( $tmpArr );
		$tmpStr = sha1( $tmpStr );
		
		if( $tmpStr == $signature ){
			echo $echostr;
		}else{
			return false;
		}
	}
	
	//easywechat微信测试接口
	public function server(){
		//$this->easywechat->server->serve()->send();
		//$response = $this->easywechat->server->serve();
		echo "<pre>";print_r($this->easywechat->server);
	}
	
}