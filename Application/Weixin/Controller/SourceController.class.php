<?php 

namespace Weixin\Controller;
use Think\Controller;

class SourceController extends BaseController{
	
	public function __construct(){
		parent::__construct();
		$this->assign('menu','source');
	}
	
	public function index(){
		
		$this->display('source');
		
	}
	
}