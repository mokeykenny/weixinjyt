<?php 

namespace Weixin\Controller;
use Think\Controller;

class DashboardController extends BaseController{
	
	public function __construct(){
		parent::__construct();
		$this->assign('menu','dashboard');
	}
	
	public function index(){
		
		$this->display('index');
		
	}
	
}