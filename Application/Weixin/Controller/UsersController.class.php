<?php 

namespace Weixin\Controller;
use Think\Controller;

class UsersController extends BaseController{
	
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		
		$this->assign('menu','users');
		$this->display('users');
		
	}
	
}