<?php 

namespace Weixin\Controller;
use EasyWeChat\Foundation\Application;
use Think\Controller;

/**
* Weixin模块controller统一入口，所有controller继承BaseController
*/
class BaseController extends Controller{
	
	public $islogin;
	public $easywechat;
	
	public function __construct(){
		
		parent::__construct();
		
		//初始化easywechat
		$options = [
			'debug'  => true,
			'app_id' => 'wxb3662fe4b09bb19c',
			'secret' => '02590ccd41a15114503985485fc43b0f',
			'token'  => 'weixin',
			// 'aes_key' => null, // 可选
			'log' => [
				'level' => 'debug',
				//'file'  => __ROOT__.'/vendor/tmp/easywechat.log', // XXX: 绝对路径！！！！
			],
			//...
		];
		$this->easywechat = new Application($options);
		
		//url跳转地址
		$app_action = C('APP_ACTION');
		//当不是单一模块绑定时，添加模块名
		if(BIND_MODULE != MODULE_NAME){
			$app_action = $app_action.MODULE_NAME.'/';
		}
		$this->islogin = true;
		$this->assign('app_action',$app_action);
		$this->assign('source_path',C('WEIXIN_SOURCE_PATH'));
		
	}
	
	public function index(){
		
		echo "抱歉，您没有权限访问！";
		
	}
	
}