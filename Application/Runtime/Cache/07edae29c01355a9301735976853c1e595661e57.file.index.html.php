<?php /* Smarty version Smarty-3.1.6, created on 2016-09-27 09:46:44
         compiled from "./Application/Weixin/View\Dashboard\index.html" */ ?>
<?php /*%%SmartyHeaderCode:3192157e9cf84587158-86701802%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '07edae29c01355a9301735976853c1e595661e57' => 
    array (
      0 => './Application/Weixin/View\\Dashboard\\index.html',
      1 => 1474180582,
      2 => 'file',
    ),
    '277e0541bfa0426a5c3cfeca062ced9a71d36372' => 
    array (
      0 => 'D:\\workspace\\weixinjyt\\Application\\Weixin\\View\\Base\\layout.html',
      1 => 1474684233,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3192157e9cf84587158-86701802',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'source_path' => 0,
    'menu' => 0,
    'app_action' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.6',
  'unifunc' => 'content_57e9cf849ec25',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57e9cf849ec25')) {function content_57e9cf849ec25($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>个人中心</title>
	<link href="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
base/images/logoico.ico" rel="Shortcut Icon">
	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
plugin/bootstrap-3.3.0-dist/dist/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
plugin/bootstrap-3.3.0-dist/dist/css/bootstrap-theme.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
plugin/font-awesome-4.6.3/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
plugin/icheck-1.x/skins/all.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
base/css/layout.css" />
	
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
dashboard/css/dashboard.css" />

	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
base/js/jquery-3.0.0.min.js" ></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
plugin/bootstrap-3.3.0-dist/dist/js/bootstrap.min.js" ></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
plugin/icheck-1.x/icheck.min.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
base/js/layout.js"></script>
</head>
<body>
	<div class="header">
		<ul class="h_ul">
			<li>
				<div class="h_logo">
					<img src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
base/images/logo2.png" />
				</div>
			</li>
		</ul>
		<div class="login_div">
			<a href="#">设置<span class="glyphicon glyphicon-cog"></span></a>
			<a href="#">登录<span class="glyphicon glyphicon-log-in"></span></a>
		</div>
	</div>
	<div class="body">
		<div class="b_aside">
			<div class="b_a_userinfo">
				<img src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
temp/userhead.jpg" />
				<span class="b_a_username">我的世界</span>
			</div>
			<ul class="b_a_mode_ul">
				<li>
					<div class="b_a_menu_line"></div>
					<div class="b_a_menu_titile">基本功能</div>
					<ul class="b_a_menu_ul">
						<li <?php if ($_smarty_tpl->tpl_vars['menu']->value=='dashboard'){?>class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['app_action']->value;?>
dashboard"><span class="glyphicon glyphicon-home"></span>个人中心</a></li>
						<li <?php if ($_smarty_tpl->tpl_vars['menu']->value=='sms'){?>class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['app_action']->value;?>
sms"><span class="glyphicon glyphicon-comment"></span>消息处理</a></li>
						<li <?php if ($_smarty_tpl->tpl_vars['menu']->value=='source'){?>class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['app_action']->value;?>
source"><span class="glyphicon glyphicon-tree-conifer"></span>素材管理</a></li>
						<li <?php if ($_smarty_tpl->tpl_vars['menu']->value=='users'){?>class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['app_action']->value;?>
users"><span class="glyphicon glyphicon-user"></span>用户管理</a></li>
						<li <?php if ($_smarty_tpl->tpl_vars['menu']->value=='plugins'){?>class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['app_action']->value;?>
plugins"><span class="glyphicon glyphicon-list"></span>插件管理</a></li>
					</ul>
				</li>
				<li>
					<div class="b_a_menu_line"></div>
					<div class="b_a_menu_titile">扩展应用</div>
					<ul class="b_a_menu_ul">
						<li><span class="glyphicon glyphicon-phone"></span>我的微站</li>
						<li><span class="glyphicon glyphicon-usd"></span>收款管理</li>
						<li><span class="glyphicon glyphicon-question-sign"></span>帮助中心</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="b_content">
			

<div class="dashboard">个人中心面板正在规划中……</div>


		</div>
	</div>
	<div class="footer">
		联系我们：老司机（90909000）
	</div>
</body>
</html><?php }} ?>