<?php /* Smarty version Smarty-3.1.6, created on 2016-09-24 10:36:06
         compiled from "./Application/Weixin/View\Users\users.html" */ ?>
<?php /*%%SmartyHeaderCode:3039557e5e568954ed3-74447571%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5cddf9481502fb3d8e5785ee93b5d67d92f9c7e6' => 
    array (
      0 => './Application/Weixin/View\\Users\\users.html',
      1 => 1474684562,
      2 => 'file',
    ),
    '277e0541bfa0426a5c3cfeca062ced9a71d36372' => 
    array (
      0 => 'D:\\workspace\\weixinjyt\\Application\\Weixin\\View\\Base\\layout.html',
      1 => 1474684233,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3039557e5e568954ed3-74447571',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.6',
  'unifunc' => 'content_57e5e568ed341',
  'variables' => 
  array (
    'source_path' => 0,
    'menu' => 0,
    'app_action' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57e5e568ed341')) {function content_57e5e568ed341($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>用户管理</title>
	<link href="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
base/images/logoico.ico" rel="Shortcut Icon">
	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
plugin/bootstrap-3.3.0-dist/dist/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
plugin/bootstrap-3.3.0-dist/dist/css/bootstrap-theme.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
plugin/font-awesome-4.6.3/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
plugin/icheck-1.x/skins/all.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
base/css/layout.css" />
	
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
users/css/users.css" />

	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
base/js/jquery-3.0.0.min.js" ></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
plugin/bootstrap-3.3.0-dist/dist/js/bootstrap.min.js" ></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
plugin/icheck-1.x/icheck.min.js"></script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
base/js/layout.js"></script>
	
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
users/js/users.js"></script>

</head>
<body>
	<div class="header">
		<ul class="h_ul">
			<li>
				<div class="h_logo">
					<img src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
base/images/logo2.png" />
				</div>
			</li>
		</ul>
		<div class="login_div">
			<a href="#">设置<span class="glyphicon glyphicon-cog"></span></a>
			<a href="#">登录<span class="glyphicon glyphicon-log-in"></span></a>
		</div>
	</div>
	<div class="body">
		<div class="b_aside">
			<div class="b_a_userinfo">
				<img src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
temp/userhead.jpg" />
				<span class="b_a_username">我的世界</span>
			</div>
			<ul class="b_a_mode_ul">
				<li>
					<div class="b_a_menu_line"></div>
					<div class="b_a_menu_titile">基本功能</div>
					<ul class="b_a_menu_ul">
						<li <?php if ($_smarty_tpl->tpl_vars['menu']->value=='dashboard'){?>class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['app_action']->value;?>
dashboard"><span class="glyphicon glyphicon-home"></span>个人中心</a></li>
						<li <?php if ($_smarty_tpl->tpl_vars['menu']->value=='sms'){?>class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['app_action']->value;?>
sms"><span class="glyphicon glyphicon-comment"></span>消息处理</a></li>
						<li <?php if ($_smarty_tpl->tpl_vars['menu']->value=='source'){?>class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['app_action']->value;?>
source"><span class="glyphicon glyphicon-tree-conifer"></span>素材管理</a></li>
						<li <?php if ($_smarty_tpl->tpl_vars['menu']->value=='users'){?>class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['app_action']->value;?>
users"><span class="glyphicon glyphicon-user"></span>用户管理</a></li>
						<li <?php if ($_smarty_tpl->tpl_vars['menu']->value=='plugins'){?>class="active"<?php }?>><a href="<?php echo $_smarty_tpl->tpl_vars['app_action']->value;?>
plugins"><span class="glyphicon glyphicon-list"></span>插件管理</a></li>
					</ul>
				</li>
				<li>
					<div class="b_a_menu_line"></div>
					<div class="b_a_menu_titile">扩展应用</div>
					<ul class="b_a_menu_ul">
						<li><span class="glyphicon glyphicon-phone"></span>我的微站</li>
						<li><span class="glyphicon glyphicon-usd"></span>收款管理</li>
						<li><span class="glyphicon glyphicon-question-sign"></span>帮助中心</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="b_content">
			

<div class="users">
	<div class="u_title">
		<span class="glyphicon glyphicon-th-large"></span>
		<span>用户管理</span>
	</div>
	<div class="u_content">
		<div class="left_content">
			<div class="top_toobar">
				<button type="button" class="btn btn-success">新建分组</button>
			</div>
			<div class="group_out">
				<ul class="group_ul">
					<li class="group_all active"><a href="#">全部用户</a></li>
					<li><a href="#">星标用户（5）</a></li>
					<li><a href="#">我的好友（2）</a></li>
					<li><a href="#">我的客户（7）</a></li>
					<li><a href="#">黑名单（1）</a></li>
				</ul>
			</div>
		</div>
		<div class="right_content">
			<div class="ude_search_input">
				<input name="nickname" class="form-control" type="text" placeholder="用户昵称" />
				<span class="glyphicon glyphicon-search"></span>
			</div>
			<div class="group_table">
				<div class="t_head">
					<input type="checkbox" name="select_all" id="select_all" />
					<label for="select_all">全选</label>
					<div class="t_head_toolbar">
					
						<button type="button" id="setLabels" class="btn btn-sm btn-default"
								data-toggle="popover" data-container="body"  data-placement="bottom" 
								data-html="true" >设置标签</button>
						<button type="button" id="setBlackList" class="btn btn-sm btn-default"
							data-container="body" data-toggle="popover" data-placement="bottom"
							data-content="已加入黑名单">加入黑名单</button>
						
						<div id="setLabels_dialog_content" class="hide">
							<div class="form-group">
								<input type="text" class="form-control" />
							</div>
							<div class="form-group" align="center">
								<button type="button" class="btn btn-success btn-sm" style="width: 100px;" data-container = "body" name="popoverBtn">确定</button>
								<button type="button" class="btn btn-default btn-sm" style="width: 100px;" name="popoverBtn">取消</button>
							</div>
						</div>
					</div>
				</div>
				<div class="t_body">
					<ul class="t_list_ul">
						<li>
							<input type="checkbox" name="select"/>
							<img src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
temp/userhead.jpg" />
							<div class="u_info">
								<div class="u_name">永和豆浆</div>
								<div class="u_label">
									<span>无标签</span>
									<i class="fa fa-caret-down"></i>
								</div>
							</div>
							<div class="u_toolbar">
								<button class="btn btn-default btn-sm">修改备注</button>
							</div>
						</li>
						<li>
							<input type="checkbox" name="select"/>
							<img src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
temp/userhead.jpg" />
							<div class="u_info">
								<div class="u_name">老面包子</div>
								<div class="u_label">
									<span class="u_label_span">我的好友</span>
									<span class="u_label_span">星标用户</span>
									<i class="fa fa-caret-down"></i>
								</div>
							</div>
							<div class="u_toolbar">
								<button class="btn btn-default btn-sm">修改备注</button>
							</div>
						</li>
						<li>
							<input type="checkbox" name="select"/>
							<img src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
temp/userhead.jpg" />
							<div class="u_info">
								<div class="u_name">三大炮</div>
								<div class="u_label">
									<span class="u_label_span">星标用户</span>
									<i class="fa fa-caret-down"></i>
								</div>
							</div>
							<div class="u_toolbar">
								<button class="btn btn-default btn-sm">修改备注</button>
							</div>
						</li>
						<li>
							<input type="checkbox" name="select"/>
							<img src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
temp/userhead.jpg" />
							<div class="u_info">
								<div class="u_name">大魔王</div>
								<div class="u_label">
									<span class="u_label_span">我的好友</span>
									<span class="u_label_span">星标用户</span>
									<i class="fa fa-caret-down"></i>
								</div>
							</div>
							<div class="u_toolbar">
								<button class="btn btn-default btn-sm">修改备注</button>
							</div>
						</li>
						<li>
							<input type="checkbox" name="select"/>
							<img src="<?php echo $_smarty_tpl->tpl_vars['source_path']->value;?>
temp/userhead.jpg" />
							<div class="u_info">
								<div class="u_name">无名girl</div>
								<div class="u_label">
									<span class="u_label_span">我的好友</span>
									<span class="u_label_span">星标用户</span>
									<span class="u_label_span">仇人</span>
									<i class="fa fa-caret-down"></i>
								</div>
							</div>
							<div class="u_toolbar">
								<button class="btn btn-default btn-sm">修改备注</button>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>


		</div>
	</div>
	<div class="footer">
		联系我们：老司机（90909000）
	</div>
</body>
</html><?php }} ?>