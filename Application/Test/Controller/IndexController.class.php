<?php 

namespace Test\Controller;
use Think\Controller;

class IndexController extends Controller{
	
	public function index(){
		
		echo "默认调用的方法";
		
	}
	
	public function _before_show(){
		
		echo "触发前置操作<br/>";
		
	}
	
	public function show($id=0){
		
		//$userM = D('user');
		$userM = M('user');
		//$User = new \Test\Model\UserModel();
		$user_info = $userM->select();
		print_r($user_info);
		
		$this->assign($user_info[0]);
		
		//echo $this->fetch("add");
		$this->display("add");
		
	}
	
	public function _after_show(){
		
		echo "<br/>触发后置操作<br/>";
		
	}
}